#include "pitbackend.h"
#include <QRegularExpression>
#include <QtNetwork>
#include <QMessageBox>

PitBackEnd::PitBackEnd(QObject *parent) :
    QObject(parent)
{
//    udpSocket = new QUdpSocket(this);
//    udpSocket->bind(45454, QUdpSocket::ShareAddress);
//    connect(udpSocket, SIGNAL(readyRead()),
//            this, SLOT(processPendingDatagrams()));
    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired) {
        // Get saved network configuration
        QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
        settings.beginGroup(QLatin1String("QtNetwork"));
        const QString id = settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
        settings.endGroup();
        // If the saved network configuration is not currently discovered use the system default
        QNetworkConfiguration config = manager.configurationFromIdentifier(id);
        if ((config.state() & QNetworkConfiguration::Discovered) !=
            QNetworkConfiguration::Discovered) {
            config = manager.defaultConfiguration();
        }
        m_networkSession = new QNetworkSession(config, this);
        connect(m_networkSession, &QNetworkSession::opened, this, &PitBackEnd::sessionOpened);
        m_networkSession->open();
    } else {
        sessionOpened();
    }
    //connect(tcpServer, &QTcpServer::newConnection, this, &PitBackEnd::sendFortune);
}
void PitBackEnd::sessionOpened()
{
    // Save the used configuration
    if (m_networkSession) {
        QNetworkConfiguration config = m_networkSession->configuration();
        QString id;
        if (config.type() == QNetworkConfiguration::UserChoice)
            id = m_networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
        else
            id = config.identifier();
        QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
        settings.beginGroup(QLatin1String("QtNetwork"));
        settings.setValue(QLatin1String("DefaultNetworkConfiguration"), id);
        settings.endGroup();
    }
//    m_tcpServer = new QTcpServer(this);
//    if (!m_tcpServer->listen(QHostAddress::Any,m_port)) {
//        QMessageBox::critical((QWidget *)this, tr("Pit Server"),
//                              tr("Unable to start the server: %1.")
//                              .arg(m_tcpServer->errorString())
//                              ,QMessageBox::Cancel);
//        return;
//    }
//    QString ipAddress;
//    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
//    // use the first non-localhost IPv4 address
//    for (int i = 0; i < ipAddressesList.size(); ++i) {
//        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
//            ipAddressesList.at(i).toIPv4Address()) {
//            ipAddress = ipAddressesList.at(i).toString();
//            break;
//        }
//    }
//    ipAddress="10.147.20.148";
//    // if we did not find one, use IPv4 localhost
//    if (ipAddress.isEmpty()) ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
//    m_status=(tr("The server is running on\n\nIP: %1\nport: %2\n\n")
//                 .arg(ipAddress).arg(m_tcpServer->serverPort()));
//    m_tcpSocket=m_tcpServer->nextPendingConnection();
//    emit statusUpdated();
////    connect (m_tcpSocket, &QIODevice::readyRead,this, &PitBackEnd::tcpRead);
////    connect (m_tcpSocket, SIGNAL(QIODevice::readyRead()),this, SLOT(tcpRead()), Qt::DirectConnection);
//    //force a read of the TCP input... should have been triggered by the signal/slot...
//    m_checkTCP = new QTimer(this);
//    connect(m_checkTCP, SIGNAL(timeout()), this, SLOT(checkTCPSocket()));
//    m_checkTCP->start(500);
}
void PitBackEnd::checkTCPSocket()
{
   if (!m_tcpSocket)
   {
       m_tcpSocket=m_tcpServer->nextPendingConnection();
       connect (m_tcpSocket, &QIODevice::readyRead,this, &PitBackEnd::tcpRead);
       return;
   }
   //connected... turn off timer
   //m_checkTCP->stop();
   //connect (m_tcpSocket, &QIODevice::readyRead,this, &PitBackEnd::tcpRead);
}
QString PitBackEnd::userName()
{
    return m_userName;
}
void PitBackEnd::setUserName(const QString &userName)
{
    if (userName == m_userName)
        return;
    m_userName = userName;
    emit userNameChanged();
}
QString PitBackEnd::readStatus()
{
    return m_status;
}
void PitBackEnd::tcpRead()
{
    //read some shit here...
    while (m_tcpSocket->canReadLine())
    {
        QByteArray ba=m_tcpSocket->readLine();
        QRegularExpression regexCheck("check...");
        QRegularExpressionMatch match=regexCheck.match(ba);
        if (match.hasMatch())
        {   //check received and responded...
            m_tcpSocket->write("OK");
            return;
        }
        QRegularExpression regex("Dat(?<dat>-?\\d+/\\d+/\\d+)Tim(?<tim>-?\\d+:\\d+:\\d+:\\d+)Lat(?<lat>-?\\d+.\\d+)Lon(?<lon>-?\\d+.\\d+)Cad(?<cad>-?\\d+)Spe(?<spe>\\d+)Set(?<set>\\d+)Thr(?<thr>\\d+)Cur(?<cur>\\d+)Tem(?<tem>\\d+)XYZ");
        QRegularExpressionMatch match1 = regex.match(ba);
        if (match1.hasMatch()){
            QString matchedTempValue = match1.captured("dat");
            matchedTempValue.append(match1.captured("tim"));
            QDateTime dateTime2 = QDateTime::fromString(matchedTempValue,"dd/MM/yyyyhh:mm:ss:zzz");
            m_currentData.timestamp= dateTime2;
            matchedTempValue = match1.captured("lat");
            m_currentData.latitude=matchedTempValue.toDouble();
            matchedTempValue = match1.captured("lon");
            m_currentData.longitude=matchedTempValue.toDouble();
            matchedTempValue = match1.captured("cad");
            m_currentData.cadence=matchedTempValue.toInt();
            matchedTempValue = match1.captured("spe");
            m_currentData.speed=matchedTempValue.toInt();
            matchedTempValue = match1.captured("set");
            m_currentData.setpoint=matchedTempValue.toInt();
            matchedTempValue = match1.captured("thr");
            m_currentData.current =matchedTempValue.toInt();
            matchedTempValue = match1.captured("cur");
            m_currentData.throttle=matchedTempValue.toInt();
            matchedTempValue = match1.captured("tem");
            m_currentData.temperature=matchedTempValue.toInt();
            emit dataUpdated();
        }
    }
}
void PitBackEnd::processPendingDatagrams()
{
    //QByteArray datagram;
    while (udpSocket->hasPendingDatagrams()) {
        m_datagram.resize(int(udpSocket->pendingDatagramSize()));
        udpSocket->readDatagram(m_datagram.data(), m_datagram.size());
        QString buff=m_datagram.data();
//        QRegularExpression regex("Dat(?<dat>-?\\d+)Tim(?<tim>-?\\d+)Lat(?<lat>-?\\d+)Lon(?<lon>-?\\d+)Cad(?<cad>-?\\d+)Spe(?<spe>\\d+)Set(?<set>\\d+)Thr(?<thr>\\d+)Cur(?<cur>\\d+)Tem(?<tem>\\d+)XYZ");
        QRegularExpression regex("Dat(?<dat>-?\\d+/\\d+/\\d+)Tim(?<tim>-?\\d+:\\d+:\\d+:\\d+)Lat(?<lat>-?\\d+.\\d+)Lon(?<lon>-?\\d+.\\d+)Cad(?<cad>-?\\d+)Spe(?<spe>\\d+)Set(?<set>\\d+)Thr(?<thr>\\d+)Cur(?<cur>\\d+)Tem(?<tem>\\d+)XYZ");
        QRegularExpressionMatch match1 = regex.match(buff);
        if (match1.hasMatch()){
            QString matchedTempValue = match1.captured("dat");
            matchedTempValue.append(match1.captured("tim"));
           // QDateTime dateTime2 = QDateTime::fromString("M1d1y9800:01:02","'M'M'd'd'y'yyhh:mm:ss");
            QDateTime dateTime2 = QDateTime::fromString(matchedTempValue,"dd/MM/yyyyhh:mm:ss:zzz");
            m_currentData.timestamp= dateTime2;
            matchedTempValue = match1.captured("lat");
            m_currentData.latitude=matchedTempValue.toDouble();
            matchedTempValue = match1.captured("lon");
            m_currentData.longitude=matchedTempValue.toDouble();
            matchedTempValue = match1.captured("cad");
            m_currentData.cadence=matchedTempValue.toInt();
            matchedTempValue = match1.captured("spe");
            m_currentData.speed=matchedTempValue.toInt();
            matchedTempValue = match1.captured("set");
            m_currentData.setpoint=matchedTempValue.toInt();
            matchedTempValue = match1.captured("thr");
            m_currentData.current =matchedTempValue.toInt();
            matchedTempValue = match1.captured("cur");
            m_currentData.throttle=matchedTempValue.toInt();
            matchedTempValue = match1.captured("tem");
            m_currentData.temperature=matchedTempValue.toInt();
            emit dataUpdated();
        }
    }
//emit receivedDatagram();
//        statusLabel->setText(tr("Received datagram: \"%1\"")
//                             .arg(datagram.constData()));
}
int PitBackEnd::readCurrent()
{
    return m_currentData.current;
}
int PitBackEnd::readOutput()
{
    return m_currentData.setpoint;
}
int PitBackEnd::readCadence()
{
    return m_currentData.cadence;
}
int PitBackEnd::readSpeed()
{
    return m_currentData.speed;
}
int PitBackEnd::readTemperature()
{
    return m_currentData.temperature;
}
double PitBackEnd::readLatitude()
{
    return m_currentData.latitude;
}
double PitBackEnd::readLongitude()
{
    return m_currentData.longitude;
}
void PitBackEnd::setPort(int &port)
{
    m_port=port;
    m_tcpServer = new QTcpServer(this);
    if (!m_tcpServer->listen(QHostAddress::Any,m_port)) {
        QMessageBox::critical((QWidget *)this, tr("Pit Server"),
                              tr("Unable to start the server: %1.")
                              .arg(m_tcpServer->errorString())
                              ,QMessageBox::Cancel);
        return;
    }
    QString ipAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    ipAddress="10.147.20.148";
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty()) ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    m_status=(tr("The server is running on\n\nIP: %1\nport: %2\n\n")
                 .arg(ipAddress).arg(m_tcpServer->serverPort()));
    m_tcpSocket=m_tcpServer->nextPendingConnection();
    emit statusUpdated();
//    connect (m_tcpSocket, &QIODevice::readyRead,this, &PitBackEnd::tcpRead);
//    connect (m_tcpSocket, SIGNAL(QIODevice::readyRead()),this, SLOT(tcpRead()), Qt::DirectConnection);
    //force a read of the TCP input... should have been triggered by the signal/slot...
    m_checkTCP = new QTimer(this);
    connect(m_checkTCP, SIGNAL(timeout()), this, SLOT(checkTCPSocket()));
    m_checkTCP->start(500);
}


//    datagram	"Vehicle1:Dat25/08/2018Tim11:44:47:817Lat-37.794200650830369Long145.057280262005122Cad0Spe0Set0Thr0Cur0Tem1\n"
//void BackEnd::DataHandler(const QString &s)
//{
//    m_BlueBuffer.append(s);
//    bool a_match;
//    /* We get a string "TempNNNNHumNNNNXYZ" (N: Number from 1 to 9)  */
//    /* We get a string CadNNNSpeNNSetNNNThrNNNCurNNNTemNNN(N: Number from 1 to 9)  */
//    //QRegularExpression re ("Temp(?<temp>-?\\d+)Hum(?<hum>\\d+)XYZ");
//    do
//    {
//        a_match=false;
//        QRegularExpression re1("Cad(?<cad>-?\\d+)Spe(?<spe>\\d+)Set(?<set>\\d+)Thr(?<thr>\\d+)Cur(?<cur>\\d+)Tem(?<tem>\\d+)Voi(?<voi>\\d+)XYZ");
//        QRegularExpressionMatch match1 = re1.match(m_BlueBuffer);
//        if (match1.hasMatch())
//        {
//            a_match=true;
//            int startOffset = match1.capturedStart(1);
//            QString captured= match1.captured(0);
//            //int diffOffset = endOffset-startOffset-1;
//            m_BlueBuffer.replace(0,startOffset+captured.length(),""); //strip out the matched data from the buffer
//            QString matchedTempValue = match1.captured("cad");
//            m_currentData.cadence=matchedTempValue.toInt();
//            matchedTempValue = match1.captured("spe");
//            m_currentData.speed=matchedTempValue.toInt();
//            matchedTempValue = match1.captured("set");
//            m_currentData.setpoint=matchedTempValue.toInt();
//            matchedTempValue = match1.captured("thr");
//            m_currentData.throttle=matchedTempValue.toInt();
//            matchedTempValue = match1.captured("cur");
//            m_currentData.current=matchedTempValue.toInt();
//            matchedTempValue = match1.captured("tem");
//            m_currentData.temperature=matchedTempValue.toInt();
//            matchedTempValue = match1.captured("voi");
//            m_currentData.voice=matchedTempValue.toInt();
//            emit mainDataChanged();
//            writeToFile();
//            if (m_currentData.voice){
//                emit togglePhone();  //call or hang up...
//            }
//            //CDG: also... if we have a match, then this should be sent on to the pit over VPN...
//        }
//    }while(a_match);
//}

