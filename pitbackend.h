#ifndef PITBACKEND_H
#define PITBACKEND_H
#include <QObject>
#include <QString>
#include <QUdpSocket>
#include <QTcpSocket>
#include <QDateTime>
#include <QTimer>

//QT_BEGIN_NAMESPACE
//class QUdpSocket;
//QT_END_NAMESPACE
QT_BEGIN_NAMESPACE
class QTcpServer;
class QNetworkSession;
QT_END_NAMESPACE
class main_data
{
public:
    QDateTime timestamp;
    double latitude;
    double longitude;
    int cadence;
    int speed;
    int setpoint;
    int throttle;
    int current;
    int temperature;
};
class PitBackEnd : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(int current READ readCurrent NOTIFY dataUpdated)
    Q_PROPERTY(int output READ readOutput)
    Q_PROPERTY(int cadence READ readCadence)
    Q_PROPERTY(int speed READ readSpeed)
    Q_PROPERTY(int temperature READ readTemperature)
    Q_PROPERTY(double latitude READ readLatitude)
    Q_PROPERTY(double longitude READ readLongitude)
    Q_PROPERTY(int port WRITE setPort)
    Q_PROPERTY(QString status READ readStatus NOTIFY statusUpdated)

//    Q_PROPERTY(QString datagram READ readDatagram  NOTIFY receivedDatagram)
public:
    explicit PitBackEnd(QObject *parent = nullptr);
    QString userName();
    int readCurrent();
    int readOutput();
    int readCadence();
    int readSpeed();
    int readTemperature();
    double readLatitude();
    double readLongitude();
    void setUserName(const QString &userName);
    QString readStatus();
    void setPort(int &port);
//    QString readDatagram();

signals:
    void userNameChanged();
    void dataUpdated();
    void statusUpdated();
//    void receivedDatagram();

private slots:
    void processPendingDatagrams();
    void sessionOpened();
    void checkTCPSocket();
    void tcpRead();
    //void sendFortune();

private:
    QUdpSocket *udpSocket = nullptr;
    QTcpServer *m_tcpServer = nullptr;
    int m_port=0;
    QNetworkSession *m_networkSession = nullptr;
    QTcpSocket *m_tcpSocket;
    QString m_userName;
    QString m_status="Not Initialised";
    QByteArray m_datagram;
    main_data m_currentData;
    QTimer *m_checkTCP;

};
#endif // PITBACKEND_H
