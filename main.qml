import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtLocation 5.3
import io.qt.examples.pitbackend 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 800
    height: 600
    title: qsTr("Tabs")
    property real gaugeScaling: 0.85
    property real gaugeScalingSmaller: 0.35
    property double vehicle1Latitude
    property double vehicle1Longitude
    property double vehicle2Latitude
    property double vehicle2Longitude
    // Oswin Street map.svg
    //TL: -37.792434,145.054623
    //BR: -37.795947,145.058478
    property double tllat: -37.792434
    property double tllon: 145.054623
    property double brlat: -37.795947
    property double brlon: 145.058478
//    maryborough-racv-track.svg
//    property double tllat: -37.040725
//    property double tllon: 143.741308
//    property double brlat: -37.048312
//    property double brlon: 143.746104
//    hawthorn-veoldrome.svg
//    property double tllat: -37.837386
//    property double tllon: 145.038032
//    property double brlat: -37.839104
//    property double brlon: 145.040739
//    tgs.svg
//    property double tllat: -37.808914
//    property double tllon: 145.028808
//    property double brlat: -37.814718
//    property double brlon: 145.036852
    property double latspan: brlat-tllat
    property double lonspan: brlon - tllon
//    property double lat: -37.044
//    property double lon: 143.744
    property int V1xpos
    property int V1ypos
    property int imageWidth: gpsMap.width    //456
    property int imageHeight: gpsMap.height  //433

    function calculateV1Marker()
    {
        gpsMap.markerV1x=(((vehicle1Latitude-tllat)/latspan)*imageWidth)
        gpsMap.markerV1y=(((vehicle1Longitude-tllon)/lonspan)*imageHeight)
//        p1.markerV1x=xpos
//        p1.markerV1y=ypos
    }
    function calculateV2Marker()
    {
        gpsMap.markerV2x=(((vehicle2Latitude-tllat)/latspan)*imageWidth)
        gpsMap.markerV2y=(((vehicle2Longitude-tllon)/lonspan)*imageHeight)
    }
    PitBackEnd {
        id: pitbackend
        port: 5100
        onDataUpdated: {
            currentGauge.value=pitbackend.current
            outputGauge.value=pitbackend.output
            cadenceGauge.value=pitbackend.cadence
            speedGauge.value=pitbackend.speed
            tempGauge.value=pitbackend.temperature/100
            vehicle1Latitude=pitbackend.latitude
            vehicle1Longitude=pitbackend.longitude
            calculateV1Marker()
        }
        onStatusUpdated: serverStatus.text = pitbackend.status
    }
    PitBackEnd {
        id: pitbackend2
        port: 5101
        onDataUpdated: {
            currentGauge2.value=pitbackend2.current
            outputGauge2.value=pitbackend2.output
            cadenceGaug2e.value=pitbackend2.cadence
            speedGauge2.value=pitbackend2.speed
            tempGauge2.value=pitbackend2.temperature/100
            vehicle2Latitude=pitbackend2.latitude
            vehicle2Longitude=pitbackend2.longitude
            calculateV2Marker()
        }
        onStatusUpdated: serverStatus2.text = pitbackend2.status
    }

//    text: pitbackend.userName
//    placeholderText: qsTr("User name")
//    anchors.centerIn: parent
//    onTextChanged: pitbackend.userName = text

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Item {
            id: page1
            Item {
                id: container
                width: root.width
                height: Math.min(root.width, root.height)
                anchors.centerIn: parent
                //Row {
                Column{
                    id: vehicle1Gauges
                    //vehicle 1
                    property color vehicle1Color: Qt.rgba(0.66, 0.0, 0.0, 0.66)
                    width: root.width/5
                    spacing: 5 //width * 0.02
                    topPadding: 30
                    Rectangle {  //this is the vehicle colour identifying circle
                         width: parent.width/12
                         height: width
//                         color: "red"
                         color: Qt.lighter(vehicle1Gauges.vehicle1Color)
                         border.color: "black"
                         border.width: 1
                         radius: width*0.5
                         x: parent.width/2 - radius
                    }
                    CircularGauge { //current
                        id: currentGauge
                        value: .9
                        maximumValue: 1
                        width: parent.width
                        height: parent.width * gaugeScalingSmaller //- gaugeRow.spacing
                        style: IconGaugeStyle {
                            id: fuelGaugeStyle
                            icon: "qrc:/icons/current2.png"
                            maxWarningColor: Qt.rgba(0.5, 0, 0, 1)
                            needleColor: vehicle1Gauges.vehicle1Color
                            tickmarkLabel: Text {
                                color: "white"
                                visible: styleData.value === 0 || styleData.value === 1
                                font.pixelSize: fuelGaugeStyle.toPixels(0.225)
                                text: styleData.value === 0 ? "0" : (styleData.value === 1 ? "20" : "")
                            }
                        }
                    }
                    CircularGauge { //output
                        id: outputGauge
                        value: .7
                        maximumValue: 1
    //                    width: parent.width
    //                    height: parent.height * 0.7
    //                    y: parent.height / 2 + container.height * 0.01
                        width: parent.width
                        height: parent.width * gaugeScalingSmaller //- gaugeRow.spacing
                        //height: width

                        style: IconGaugeStyle {
                            id: outputGaugeStyle
                            minorTickmarkCount: 2
                            icon: "qrc:/icons/output2.png"
                            maxWarningColor: Qt.rgba(0.5, 0, 0, 1)
                            needleColor: vehicle1Gauges.vehicle1Color
                            tickmarkLabel: Text {
                                color: "white"
                                visible: styleData.value === 0 || styleData.value === 1
                                font.pixelSize: outputGaugeStyle.toPixels(0.225)
                                text: styleData.value === 0 ? "0" : (styleData.value === 1 ? "15" : "")
                            }
                        }
                    }
                    CircularGauge { //output
                        id: tempGauge
                        value: .2
                        maximumValue: 1
    //                    width: parent.width
    //                    height: parent.height * 0.7
    //                    y: parent.height / 2 + container.height * 0.01
                        width: parent.width
                        height: parent.width * gaugeScalingSmaller //- gaugeRow.spacing
                        //height: width

                        style: IconGaugeStyle {
                            id: tempGaugeStyle
                            minorTickmarkCount: 3
                            icon: "qrc:/icons/temp.png"
                            maxWarningColor: Qt.rgba(0.5, 0, 0, 1)
                            needleColor: vehicle1Gauges.vehicle1Color
                            tickmarkLabel: Text {
                                color: "white"
                                visible: styleData.value === 0 || styleData.value === 1
                                font.pixelSize: tempGaugeStyle.toPixels(0.225)
                                text: styleData.value === 0 ? "0" : (styleData.value === 1 ? "100" : "")
                            }
                        }
                    }
                    CircularGauge { //cadence
                        id: cadenceGauge
                        width: parent.width
                        height: parent.width * gaugeScalingSmaller //- gaugeRow.spacing
                        value: 60  //valueSource.rpm
                        maximumValue: 120
                     //   anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        Behavior on value {SpringAnimation{spring: 2; damping: 0.2}}

                        style: TachometerStyle {
                            needleColor: vehicle1Gauges.vehicle1Color
//                            Image {
//                                source: "qrc:/icons/cadence2.png"
//                                anchors.bottom: parent.verticalCenter
//                                anchors.bottomMargin: parent.width*0.03
//                                anchors.horizontalCenter: parent.horizontalCenter
//                                width: parent.width*0.08
//                                height: width
//                                fillMode: Image.PreserveAspectFit
//                            }
                        }
                    }
                    CircularGauge { //speed
                        id: speedGauge
                        value: 40
                       // anchors.verticalCenter: parent.verticalCenter
                        //anchors.top: tachometer.bottom
                        maximumValue: 70
                        width: parent.width
                        height: parent.width * gaugeScaling
                        anchors.horizontalCenter: parent.horizontalCenter
                        Behavior on value {SpringAnimation{spring: 2; damping: 0.2}}
                        style: DashboardGaugeStyle {
                        needleColor: vehicle1Gauges.vehicle1Color
                        }
                    }
                }
                Column{
                    id: mapColumn
                    anchors.left: vehicle1Gauges.right
                    width: 3*root.width/5
                    height: parent.height
                    spacing: 5 //width * 0.02
                    topPadding: 60
                    Image {
                        id: gpsMap
                        property alias markerV1x: markerV1.x
                        property alias markerV1y: markerV1.y
                        property alias markerV2x: markerV2.x
                        property alias markerV2y: markerV2.y
                        source: "qrc:/maps/Oswin Street map.svg"
                        transformOrigin: Item.TopLeft
                        anchors.horizontalCenter: parent.horizontalCenter
                        fillMode: Image.PreserveAspectFit
                        Rectangle {
                            id: markerV1
                            x: 0
                            y: 0
                            width: 14
                            height: 14
                            color: vehicle1Gauges.vehicle1Color
                            //color: "#ff0000"
                            radius: 7
                            transformOrigin: Item.Center
                        }
                        Rectangle {
                            id: markerV2
                            x: 0
                            y: 0
                            width: 14
                            height: 14
                            color: vehicle2Gauges.vehicle2Color
                            //color: "#ff0000"
                            radius: 7
                            transformOrigin: Item.Center
                        }
                    }
                }
                Column{
                    id: vehicle2Gauges
                    //vehicle 2
                    anchors.left: mapColumn.right
                    property color vehicle2Color: Qt.rgba(0.0, 0.66, 0.0, 0.66)
                    width: root.width/5
                    spacing: 5 //width * 0.02
                    topPadding: 30
                    Rectangle {  //this is the vehicle colour identifying circle
                         width: parent.width/12
                         height: width
//                         color: "red"
                         color: Qt.lighter(vehicle2Gauges.vehicle2Color)
                         border.color: "black"
                         border.width: 1
                         radius: width*0.5
                         x: parent.width/2 - radius
                    }
                    CircularGauge { //current
                        id: currentGauge2
                        value: .9
                        maximumValue: 1
                        width: parent.width
                        height: parent.width * gaugeScalingSmaller //- gaugeRow.spacing
                        style: IconGaugeStyle {
                            id: fuelGaugeStyle2
                            icon: "qrc:/icons/current2.png"
                            maxWarningColor: Qt.rgba(0.5, 0, 0, 1)
                            needleColor: vehicle2Gauges.vehicle2Color
                            tickmarkLabel: Text {
                                color: "white"
                                visible: styleData.value === 0 || styleData.value === 1
                                font.pixelSize: fuelGaugeStyle2.toPixels(0.225)
                                text: styleData.value === 0 ? "0" : (styleData.value === 1 ? "20" : "")
                            }
                        }
                    }
                    CircularGauge { //output
                        id: outputGauge2
                        value: .7
                        maximumValue: 1
    //                    width: parent.width
    //                    height: parent.height * 0.7
    //                    y: parent.height / 2 + container.height * 0.01
                        width: parent.width
                        height: parent.width * gaugeScalingSmaller //- gaugeRow.spacing
                        //height: width

                        style: IconGaugeStyle {
                            id: outputGaugeStyle2
                            minorTickmarkCount: 2
                            icon: "qrc:/icons/output2.png"
                            maxWarningColor: Qt.rgba(0.5, 0, 0, 1)
                            needleColor: vehicle2Gauges.vehicle2Color
                            tickmarkLabel: Text {
                                color: "white"
                                visible: styleData.value === 0 || styleData.value === 1
                                font.pixelSize: outputGaugeStyle2.toPixels(0.225)
                                text: styleData.value === 0 ? "0" : (styleData.value === 1 ? "15" : "")
                            }
                        }
                    }
                    CircularGauge { //output
                        id: tempGauge2
                        value: .2
                        maximumValue: 1
    //                    width: parent.width
    //                    height: parent.height * 0.7
    //                    y: parent.height / 2 + container.height * 0.01
                        width: parent.width
                        height: parent.width * gaugeScalingSmaller //- gaugeRow.spacing
                        //height: width

                        style: IconGaugeStyle {
                            id: tempGaugeStyle2
                            minorTickmarkCount: 3
                            icon: "qrc:/icons/temp.png"
                            maxWarningColor: Qt.rgba(0.5, 0, 0, 1)
                            needleColor: vehicle2Gauges.vehicle2Color
                            tickmarkLabel: Text {
                                color: "white"
                                visible: styleData.value === 0 || styleData.value === 1
                                font.pixelSize: tempGaugeStyle2.toPixels(0.225)
                                text: styleData.value === 0 ? "0" : (styleData.value === 1 ? "100" : "")
                            }
                        }
                    }
                    CircularGauge { //cadence
                        id: cadenceGauge2
                        width: parent.width
                        height: parent.width * gaugeScalingSmaller //- gaugeRow.spacing
                        value: 60  //valueSource.rpm
                        maximumValue: 120
                     //   anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        Behavior on value {SpringAnimation{spring: 2; damping: 0.2}}

                        style: TachometerStyle {
                            needleColor: vehicle2Gauges.vehicle2Color
//                            Image {
//                                source: "qrc:/icons/cadence2.png"
//                                anchors.bottom: parent.verticalCenter
//                                anchors.bottomMargin: parent.width*0.03
//                                anchors.horizontalCenter: parent.horizontalCenter
//                                width: parent.width*0.08
//                                height: width
//                                fillMode: Image.PreserveAspectFit
//                            }
                        }
                    }
                    CircularGauge { //speed
                        id: speedGauge2
                        value: 40
                       // anchors.verticalCenter: parent.verticalCenter
                        //anchors.top: tachometer.bottom
                        maximumValue: 70
                        width: parent.width
                        height: parent.width * gaugeScaling
                        anchors.horizontalCenter: parent.horizontalCenter
                        Behavior on value {SpringAnimation{spring: 2; damping: 0.2}}
                        style: DashboardGaugeStyle {
                        needleColor: vehicle2Gauges.vehicle2Color
                        }
                    }
                }
            }
        }
//        Page1Form {
//        }

        Page2Form {
            Label{
                id: serverStatus;
                text: pitbackend.status
            }
            Label{
                id: serverStatus2;
                text: pitbackend2.status
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Page 1")
        }
        TabButton {
            text: qsTr("Page 2")
        }
    }
}

//import QtQuick 2.9
//import QtQuick.Controls 2.2

//ApplicationWindow {
//    id: window
//    visible: true
//    width: 640
//    height: 480
//    title: qsTr("Stack")

//    header: ToolBar {
//        contentHeight: toolButton.implicitHeight

//        ToolButton {
//            id: toolButton
//            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
//            font.pixelSize: Qt.application.font.pixelSize * 1.6
//            onClicked: {
//                if (stackView.depth > 1) {
//                    stackView.pop()
//                } else {
//                    drawer.open()
//                }
//            }
//        }

//        Label {
//            text: stackView.currentItem.title
//            anchors.centerIn: parent
//        }
//    }

//    Drawer {
//        id: drawer
//        width: window.width * 0.66
//        height: window.height

//        Column {
//            anchors.fill: parent

//            ItemDelegate {
//                text: qsTr("Page 1")
//                width: parent.width
//                onClicked: {
//                    stackView.push("Page1Form.ui.qml")
//                    drawer.close()
//                }
//            }
//            ItemDelegate {
//                text: qsTr("Page 2")
//                width: parent.width
//                onClicked: {
//                    stackView.push("Page2Form.ui.qml")
//                    drawer.close()
//                }
//            }
//        }
//    }

//    StackView {
//        id: stackView
//        initialItem: "HomeForm.ui.qml"
//        anchors.fill: parent
//    }
//}
